## Сайт по продаже услуг химчистки :)

***

Прототип сайта, созданный на основе подработки моего друга.

### Реализованы:
* Посты
* Комментарии к ним
* Услуги
* Отзывы к услугам
* Корзина, оплата
* Рассылка 

### Используемый стек на данный момент:
* Django
* DRF, Djoser, SimpleJWT
* PostgreSQL
* Celery, Redis
* DRF-YASG2
* Docker
* VueJS, Vuetify (в другом репозитории)

Установка:
1) Склонировать репозиторий
2) Создать виртуальное окружение
3) Установить зависимости `pip install -r req.txt`
4) Запустить сервер `python manage.py runserver`
5) Запустить контейнер redis `docker run -d -p 6379:6379 --name redis redis`
6) Запустить celery worker `celery -A config worker -l INFO`
