FROM python:3.8

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYCODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update \
    && apt-get install netcat -y
RUN apt-get upgrade -y && apt-get install postgresql gcc python3-dev musl-dev -y
RUN pip install --upgrade pip


COPY ./req.txt .
RUN pip install -r req.txt

RUN chmod +x ./entrypoint.sh
COPY ./entrypoint.sh .
RUN chmod +x ./entrypoint.sh

COPY . .

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]















# FROM python:3.8

# ENV PYTHONDONTWRITEBYCODE 1
# ENV PYTHONUNBUFFERED 1

# WORKDIR /usr/src/barom_project

# COPY ./req.txt /usr/src/req.txt
# RUN pip install -r /usr/src/req.txt

# COPY . /usr/src/barom_project

# EXPOSE 8000
