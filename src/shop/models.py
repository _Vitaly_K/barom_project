from django.db import models
from django.conf import settings

from ..reviews.models import Service


class Order(models.Model):
    """Модель заказа"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='orders',
        verbose_name='Пользователь'
    )
    first_name = models.CharField(max_length=100, verbose_name='Фамилия')
    last_name = models.CharField(max_length=100, verbose_name='Имя')
    order_email = models.CharField(max_length=100, verbose_name='Почта для заказа')
    address = models.CharField(max_length=100, verbose_name='Адрес')
    zipcode = models.CharField(max_length=100, verbose_name='Индекс')
    phone = models.CharField(max_length=100, verbose_name='Телефон')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания заказа')
    paid_amount = models.PositiveIntegerField(blank=True, null=True, verbose_name='Стоимость заказа')
    stripe_token = models.CharField(max_length=100, verbose_name='Токен Страйп')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['-created_at', ]

    def __str__(self):
        return f'{self.id} - {self.first_name} {self.last_name}'


class OrderService(models.Model):
    """Модель услуги для заказа"""

    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE, verbose_name='Заказ')
    service = models.ForeignKey(Service, related_name='items', on_delete=models.CASCADE, verbose_name='Услуга')
    price = models.PositiveIntegerField(verbose_name='Цена')

    class Meta:
        verbose_name = 'Услуга для заказа'
        verbose_name_plural = 'Услуги для заказов'

    def __str__(self):
        return f"{self.order} - {self.service}"
