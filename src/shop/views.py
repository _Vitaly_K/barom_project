import stripe

from django.conf import settings

from rest_framework import status, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import OrderSerializer


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def checkout(request):
    """Оплата заказа"""
    serializer = OrderSerializer(data=request.data)

    if serializer.is_valid():
        stripe.api_key = settings.STRIPE_SECRET_KEY

        try:
            charge = stripe.Charge.create(
                amount=int(serializer.validated_data['paid_amount']),
                currency='USD',
                description='Платеж Баром',
                source=serializer.validated_data['stripe_token']
            )

            serializer.save(user=request.user)

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        except Exception:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
