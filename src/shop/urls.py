from django.urls import path
from ..shop import views

urlpatterns = [
    path('checkout/', views.checkout, name='checkout')
]
