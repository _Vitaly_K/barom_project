from django.contrib import admin

from .models import OrderService, Order


class OrderAdmin(admin.ModelAdmin):
    pass


class OrderServiceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Order, OrderAdmin)

admin.site.register(OrderService, OrderServiceAdmin)
