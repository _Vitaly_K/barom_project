from rest_framework import serializers

from .models import Order, OrderService


class OrderServiceSerializer(serializers.ModelSerializer):
    """ Вывод услуги для заказа"""
    class Meta:
        model = OrderService
        fields = ('service', 'price')


class OrderSerializer(serializers.ModelSerializer):
    """Вывод заказа"""
    items = OrderServiceSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            'id',
            'first_name',
            'last_name',
            'order_email',
            'address',
            'zipcode',
            'phone',
            'stripe_token',
            'paid_amount',
            'items',
        )

    def create(self, validated_data):
        items_data = validated_data.pop('items')
        order = Order.objects.create(**validated_data)

        for item_data in items_data:
            OrderService.objects.create(order=order, **item_data)

        return order

OrderSerializer()



