from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.test import force_authenticate
from django.urls import reverse

from src.reviews.models import Service
from src.shop.models import Order, OrderService
from ..serializers import OrderServiceSerializer, OrderSerializer
from src.profile.models import BaromUser


class CheckoutAPITestCase(APITestCase):

    def setUp(self):

        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='Test1'
        )
        self.service1 = Service.objects.create(
            title='1',
            description='1',
            price=1000
        )
        self.service2 = Service.objects.create(
            title='2',
            description='2',
            price=2000
        )
        self.service3 = Service.objects.create(
            title='3',
            description='3',
            price=3000
        )

    def test_checkout(self):

        url = reverse('checkout')
        data = {
            'first_name': 'Test1',
            'last_name': 'Test1',
            'items': [
                {
                    'price': self.service1.price,
                    'service': self.service1.id
                }
            ],
            'order_email': 'test@test.com',
            'address': 'Test1',
            'zipcode': '123456',
            'phone': '89870001122',
            'paid_amount': '6999',
            'stripe_token': 'xxxxxxxxxxxxxxxxxxxx'
        }
        self.client.force_authenticate(self.user1)
        response = self.client.post(url, data)
        order = Order.objects.all()
        print(order)

        print(response.data)

