from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, F
from rest_framework import (
    generics,
    views,
    response,
    viewsets,
    permissions
)

from .serializers import (
    PostSerializer,
    CommentSerializer,
    AllPostSerializer,
    PopularPostSerializer
)
from .models import Post, PostComments
from ..services import IsAuthor, MixedPermission, Pagination


class PostListView(generics.ListAPIView):
    """Вывод всех постов"""

    serializer_class = AllPostSerializer
    pagination_class = Pagination

    def get_queryset(self):
        return Post.objects.filter(is_draft=False).prefetch_related('likedone')


class PopularPostListView(generics.ListAPIView):
    """Вывод 4 самых популярных постов"""

    serializer_class = PopularPostSerializer

    def get_queryset(self):
        return Post.objects.filter(is_draft=False).annotate(
            likes=Count('likedone')).order_by('-likes')[:4]


class PostView(generics.RetrieveAPIView):
    """Вывод одного поста"""

    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.filter(is_draft=False).select_related('user').prefetch_related('comments')


class LikeOrDislikeView(views.APIView):
    """Добавление или удаление лайка"""

    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk):
        try:
            post = Post.objects.get(id=pk)
            if request.user in post.likedone.all():
                post.likedone.remove(request.user)
                return response.Response('Вы убрали лайк', status=201)
            else:
                post.likedone.add(request.user)
                return response.Response('Вы добавили лайк', status=201)
        except ObjectDoesNotExist:
            return response.Response(status=404)


class CommentViewset(MixedPermission, viewsets.ModelViewSet):
    """ CRUD комментариев к записи"""

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = PostComments.objects.all()
    serializer_class = CommentSerializer
    permission_classes_by_action = {'update': [IsAuthor],
                                    'destroy': [IsAuthor]}

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)







