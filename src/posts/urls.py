from django.urls import path
from . import views


urlpatterns = [
    path('popular/', views.PopularPostListView.as_view(), name='populat_posts'),
    path('', views.PostListView.as_view(), name='post_list'),
    path('<int:pk>/', views.PostView.as_view(), name='post'),
    path('like_dislike/<int:pk>/', views.LikeOrDislikeView.as_view(), name='like_or_dislike'),
    path('comment/', views.CommentViewset.as_view({'post': 'create'}), name='create_comment'),
    path('comment/<int:pk>/', views.CommentViewset.as_view(
        {'put': 'update', 'delete': 'destroy'}),
         name='change_delete_comment'
         ),
]
