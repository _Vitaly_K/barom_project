from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Post, PostComments


class PostAdmin(ModelAdmin):
    pass


class PostCommentsAdmin(ModelAdmin):
    pass


admin.site.register(Post, PostAdmin)

admin.site.register(PostComments, PostCommentsAdmin)
