import json
import sys


from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.conf import settings

from src.email_sending.tasks import send_email_new_post


class Post(models.Model):
    """Модель поста (для стаффа)"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='posts',
        verbose_name='Пользователь'
    )
    title = models.CharField('Название', max_length=100)
    text = models.TextField('Текст поста')
    photo = models.ImageField('Фото', upload_to='user/post/', blank=True, null=True)
    created_at = models.DateTimeField('Пост создан', auto_now_add=True)
    is_draft = models.BooleanField('Черновик', default=False)
    likedone = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='likes', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ['-created_at']


class PostComments(models.Model):
    """Модель комментов к постам"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Пользователь'
    )
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Пост'
    )
    text = models.TextField('Текст')
    created_at = models.DateTimeField('Добавлен', auto_now_add=True)
    edited_at = models.DateTimeField('Обновлён', auto_now=True)
    is_active = models.BooleanField('Активен?', default=True)

    def __str__(self):
        return f'Комментарий от {self.user}'

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'


@receiver(post_save, sender=Post)
def post_created_handler(instance, **kwargs):
    send_email_new_post.delay(str(instance))