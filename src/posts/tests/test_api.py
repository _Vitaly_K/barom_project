from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from rest_framework.test import force_authenticate
from django.urls import reverse

from ..models import Post, PostComments
from ..serializers import AllPostSerializer, PopularPostSerializer
from src.profile.models import BaromUser


class PostAPITestCase(APITestCase):

    def setUp(self):
        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='test1'
        )
        self.user1 = BaromUser.objects.create(
            username='Test2',
            password='test2'
        )
        self.post1 = Post.objects.create(
            user=self.user1,
            title='1',
            text='1',
        )
        self.post2 = Post.objects.create(
            user=self.user1,
            title='2',
            text='2',
        )

    def test_all_posts(self):

        url = reverse('post_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_popular_posts(self):
        url = reverse('populat_posts')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_like_or_dislike(self):

        url = reverse('like_or_dislike', kwargs={ 'pk': self.post1.id })
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        user = BaromUser.objects.first()
        self.client.force_authenticate(user=user)
        response = self.client.post(url)
        self.assertEqual(response.data, 'Вы добавили лайк')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(url)
        self.assertEqual(response.data, 'Вы убрали лайк')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_one_post(self):
        url = reverse('post', kwargs={ 'pk': self.post1.id })
        response = self.client.get(url)
        expected_keys = set(['id', 'user', 'photo', 'likedone', 'comments', 'title', 'text', 'created_at'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(set(response.data.keys()), expected_keys)

    def test_create_change_delete_comment(self):

        url1 = reverse('create_comment')
        data1 = {'post': self.post1.id, 'text': 'Test_comment'}
        response = self.client.post(url1, data1)
        is_commented = Post.objects.get(id=self.post1.id).comments.exists()
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(is_commented, False)

        user1 = BaromUser.objects.get(username='Test1')
        self.client.force_authenticate(user=user1)
        response = self.client.post(url1, data1)
        is_commented = Post.objects.get(id=self.post1.id).comments.exists()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(is_commented, True)

        user2 = BaromUser.objects.get(username='Test2')
        self.client.force_authenticate(user=user2)
        comment = Post.objects.get(id=self.post1.id).comments.get(text='Test_comment')
        url2 = reverse('change_delete_comment', kwargs={ 'pk': comment.id })
        data2 = {'post': self.post1.id, 'text': 'Test_comment2'}
        response = self.client.put(url2, data2)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(user=user1)
        response = self.client.put(url2, data2)
        comment_text = Post.objects.get(id=self.post1.id).comments.get().text
        self.assertEqual(comment_text, 'Test_comment2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.delete(url2)
        is_comment_existed = PostComments.objects.exists()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(is_comment_existed, False)




