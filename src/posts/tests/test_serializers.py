from django.test import TestCase

from ..models import Post, PostComments
from src.profile.models import BaromUser
from ..serializers import PostSerializer, LikeSerializer, CommentForPostSerializer, AllPostSerializer

class PostSerialiersTestCase(TestCase):

    def setUp(self):
        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='test1'
        )
        self.post1 = Post.objects.create(
            user=self.user1,
            title='1',
            text='1',
        )
        self.post2 = Post.objects.create(
            user=self.user1,
            title='2',
            text='2',
        )
        self.comment1 = PostComments.objects.create(
            user = self.user1,
            post = self.post1,
            text = '1 comment'
        )

    def test_one_post_serializer(self):
        data = PostSerializer(self.post1).data
        expected_data_keys = set(['id', 'user', 'photo', 'likedone', 'comments', 'title', 'text', 'created_at'])
        self.assertEqual(set(data.keys()), expected_data_keys)

    def test_all_post_serializer(self):
        data = AllPostSerializer([self.post1, self.post2], many=True).data
        expected_data_keys = set(['id', 'photo', 'likedone', 'title', 'text', 'created_at'])
        self.assertEqual(set(data[0].keys()), set(data[1].keys()))
        self.assertEqual(expected_data_keys, set(data[0].keys()))


