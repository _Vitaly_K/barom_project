from django.test import TestCase

from ..models import Post, PostComments
from src.profile.models import BaromUser


class PostModelTestCase(TestCase):

    def setUp(self):
        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='test1'
        )

        self.post1 = Post.objects.create(
            user=self.user1,
            title='1',
            text='1',
        )
        self.comment1 = PostComments.objects.create(
            user = self.user1,
            post = self.post1,
            text = '1 comment'
        )

    def test_post_str_method(self):

        self.assertEqual(str(self.post1), self.post1.title)

    def test_comment_str_method(self):

        self.assertEqual(str(self.comment1), f'Комментарий от {self.user1}')

