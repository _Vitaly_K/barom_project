from rest_framework import serializers

from .models import Post, PostComments
from ..profile.models import BaromUser


class LikeSerializer(serializers.ModelSerializer):
    """Нужен для вывода пользователей, которые поставили лайк посту"""

    class Meta:
        model = BaromUser
        fields = ('username',)


class CommentForPostSerializer(serializers.ModelSerializer):
    """Нужен для вывода комментариев к посту"""

    user = serializers.ReadOnlyField(source='user.username')
    
    class Meta:
        model = PostComments
        exclude = ('is_active', )


class CommentSerializer(serializers.ModelSerializer):
    """Нужен для CRUD поста"""

    class Meta:
        model = PostComments
        fields = ('post', 'text')


class PostSerializer(serializers.ModelSerializer):
    """Вывод одного поста"""

    user = serializers.ReadOnlyField(source='user.username')
    photo = serializers.ImageField(read_only=True)
    likedone = LikeSerializer(many=True, read_only=True)
    comments = CommentForPostSerializer(many=True)

    class Meta:
        model = Post
        exclude = ('is_draft', )


class AllPostSerializer(serializers.ModelSerializer):
    """Вывод постов на главную страницу"""

    photo = serializers.ImageField(read_only=True)
    likedone = LikeSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        exclude = ('is_draft', 'user')


class PopularPostSerializer(serializers.ModelSerializer):
    """Вывод самых популярных постов"""

    photo = serializers.ImageField(read_only=True)
    likedone = LikeSerializer(many=True, read_only=True)
    likes = serializers.IntegerField()
    
    class Meta:
        model = Post
        exclude = ('is_draft', )
