from django.urls import path
from . import views


urlpatterns = [
    path('services/', views.ServiceAllView.as_view(), name='service_list'),
    path('services/search/', views.SearchView.as_view(), name='search'),
    path('services/<int:pk>/', views.ServiceView.as_view(), name='service'),
    path('', views.ReviewViewset.as_view({'post': 'create'}), name='create_review'),
    path('<int:pk>/', views.ReviewViewset.as_view(
        {'put': 'update', 'delete': 'destroy'}),
         name='change_or_delete_review'),
]
