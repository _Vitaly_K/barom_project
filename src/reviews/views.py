from rest_framework import generics, viewsets, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Avg, Q

from .serializers import (
    ReviewSerializer,
    ServiceSerializer,
    ServiceAllSerializer,
    ServiceForSearchSerializer
)
from .models import Service, Review
from ..services import MixedPermission, IsAuthor


class ServiceView(generics.RetrieveAPIView):
    """Вывод одной услуги со средним рейтингом и отзывов к ним"""

    serializer_class = ServiceSerializer

    def get_queryset(self):
        services = Service.objects.annotate(
            middle_rating=Avg('reviews__rating')
        )
        return services


class ServiceAllView(generics.ListAPIView):
    """Вывод списка услуг со средним рейтингом и отзывов к ним"""

    serializer_class = ServiceAllSerializer

    def get_queryset(self):
        services = Service.objects.annotate(
            middle_rating=Avg('reviews__rating')
        )
        return services


class ReviewViewset(MixedPermission, viewsets.ModelViewSet):
    """ CRUD отзывов"""

    serializer_class = ReviewSerializer
    permission_classes_by_action = {
        'create': [permissions.IsAuthenticated],
        'update': [IsAuthor],
        'destroy': [IsAuthor]
    }

    def get_queryset(self):
        return Review.objects.filter(is_active=True)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class SearchView(APIView):
    """Поиск среди услуг"""
    def post(self, request):
        query = request.data.get('query', '')
        if query:
            services = Service.objects.filter(Q(title__icontains=query) | Q(description__icontains=query))
            serializer = ServiceForSearchSerializer(services, many=True)
            return Response(serializer.data)
        else:
            return Response({'services': []})
