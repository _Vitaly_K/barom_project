from django.contrib import admin

from .models import Review, Service


class ReviewAdmin(admin.ModelAdmin):
    pass


class ServiceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Review, ReviewAdmin)

admin.site.register(Service, ServiceAdmin)
