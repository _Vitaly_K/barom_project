from django.db import models

from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save

from src.email_sending.tasks import send_email_new_service

RATING = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
)


class Service(models.Model):
    """Модель услуги"""

    title = models.CharField('Название услуги', max_length=50)
    description = models.TextField('Описание', max_length=200)
    price = models.PositiveIntegerField('Цена услуги')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'


class Review(models.Model):
    """Модель отзыва на услугу"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='reviews',
        verbose_name='Пользователь')
    text = models.TextField('Текст')
    photo = models.ImageField('Фотография к отзыву', upload_to='user/review/', blank=True, null=True)
    created_at = models.DateTimeField('Опубликован', auto_now_add=True)
    rating = models.PositiveIntegerField('Рейтинг', choices=RATING)
    service = models.ForeignKey(
        Service,
        on_delete=models.CASCADE,
        related_name='reviews',
        verbose_name='Услуга'
    )
    is_active = models.BooleanField('Опубликовать', default=True)

    def __str__(self):
        return f'{self.service}: {self.rating}'

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


@receiver(post_save, sender=Service)
def post_created_handler(instance, **kwargs):
    send_email_new_service.delay(str(instance))
