from rest_framework import serializers

from .models import Service, Review
from ..profile.models import BaromUser


class UserForReviewSerializer(serializers.ModelSerializer):
    """Вытащить юзера для отзывов"""
    class Meta:
        model = BaromUser
        fields = ('username', )

  
class ReviewSerializer(serializers.ModelSerializer):
    """Вывод отзывов к услуге"""

    user = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = Review
        exclude = ('is_active', )


class ServiceSerializer(serializers.ModelSerializer):
    """Вывод одной услуги"""

    middle_rating = serializers.FloatField()
    reviews = ReviewSerializer(many=True)

    class Meta:
        model = Service
        fields = '__all__'


class ServiceAllSerializer(serializers.ModelSerializer):
    """Вывод всех  услуг"""

    middle_rating = serializers.FloatField()

    class Meta:
        model = Service
        fields = '__all__'


class ServiceForSearchSerializer(serializers.ModelSerializer):
    """Вывод услуг для поиска"""

    class Meta:
        model = Service
        fields = '__all__'
