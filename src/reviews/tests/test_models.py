from django.test import TestCase

from ..models import Service, Review
from src.profile.models import BaromUser


class ServiceModelTestCase(TestCase):

    def setUp(self):
        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='test1'
        )

        self.service = Service.objects.create(
            title='1',
            description='1',
            price='1000',
        )
        self.review = Review.objects.create(
            user = self.user1,
            text = 'Test_review',
            rating = '5',
            service=self.service,
        )

    def test_service_str_method(self):

        self.assertEqual(str(self.service), self.service.title)

    def test_review_str_method(self):

        self.assertEqual(str(self.review), f'{self.service}: {self.review.rating}')