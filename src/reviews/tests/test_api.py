from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.test import force_authenticate
from django.urls import reverse

from ..models import Service, Review
from ..serializers import ServiceSerializer, ServiceAllSerializer
from src.profile.models import BaromUser


class ServiceAPITestCase(APITestCase):

    def setUp(self):

        self.user1 = BaromUser.objects.create(
            username='Test1',
            password='test1'
        )
        self.service1 = Service.objects.create(
            title='Test service',
            description='Test service',
            price='1000'
        )
        self.service2 = Service.objects.create(
            title='Test service 2',
            description='Test service 2',
            price='2000'
        )
        self.review1 = Review.objects.create(
            user = self.user1,
            text='Test review',
            rating = '1',
            service=self.service1
        )

    def test_all_services(self):

        url = reverse('service_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_search(self):

        url = reverse('search')
        data1 = { 'query':  'Test' }
        response1 = self.client.post(url, data1)

        data2 = { 'query': '' }
        response2 = self.client.post(url, data2)

        self.assertEqual(len(response1.data), 2)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data, { 'services': [] })

    def test_one_service(self):

        url = reverse('service', kwargs={ 'pk': self.service1.id })
        response = self.client.get(url)
        keys = set(response.data.keys())
        expected_keys = set(['id', 'middle_rating', 'reviews', 'title', 'description', 'price'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(keys, expected_keys)

    def test_create_review(self):

        url = reverse('create_review')
        data = { 'text': 'Test review 2', 'rating': '5', 'service': self.service1.id }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Review.objects.count(), 1)

        self.client.force_authenticate(self.user1)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Review.objects.count(), 2)

    def test_change_or_delete_review(self):

        url = reverse('change_or_delete_review', kwargs={ 'pk': self.review1.id })
        data = {'text': 'Changed review', 'rating': '5', 'service': self.service1.id }

        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Review.objects.count(), 1)

        self.client.force_authenticate(self.user1)
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Review.objects.count(), 1)

        user2 = BaromUser.objects.create(username='Test2', password='test2')
        self.client.force_authenticate(user2)
        response1 = self.client.put(url, data)
        response2 = self.client.delete(url)
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response2.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.user1)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Review.objects.count(), 0)

