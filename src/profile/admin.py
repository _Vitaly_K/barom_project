from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import BaromUser


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(BaromUser, UserAdmin)
