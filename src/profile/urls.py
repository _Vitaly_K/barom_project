from django.urls import path
from . import views


urlpatterns = [
    path('profile/<int:pk>/', views.UserViewSet.as_view(
        {'get': 'retrieve', 'put': 'update'}
    )),
]
