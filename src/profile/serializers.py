from rest_framework import serializers

from .models import BaromUser


class BaromUserSerializer(serializers.ModelSerializer):
    """Вывод инфо о пользователе"""

    avatar = serializers.ImageField(read_only=True)

    class Meta:
        model = BaromUser
        fields = (
            'username',
            'first_name',
            'last_name',
            'avatar',
            'phone_number'
        )
