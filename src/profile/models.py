from django.contrib.auth.models import AbstractUser
from django.db import models


class BaromUser(AbstractUser):
    """ Кастомная модель юзера """

    avatar = models.ImageField(upload_to='user/avatar/', blank=True, null=True)
    phone_number = models.CharField(max_length=11)
