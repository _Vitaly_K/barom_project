from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from .serializers import BaromUserSerializer
from .models import BaromUser


class UserViewSet(ModelViewSet):
    """ Вывод профиля пользователя"""

    serializer_class = BaromUserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return BaromUser.objects.filter(id=self.request.user.id)



