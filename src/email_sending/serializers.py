from rest_framework import serializers

from .models import Subscriber


class SubscriberEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscriber
        fields = '__all__'
