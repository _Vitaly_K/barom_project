import sys, os

from django.core.mail import send_mail

from .models import Subscriber
from config.celery import app


@app.task
def send_email_new_post(*args, **kwargs):
    for sub in Subscriber.objects.all():
        send_mail(
            'Опубликован пост',
            f"Опубликован пост под названием {args}."
            f"Для просмотра перейдите на наш сайт.",
            'nopro.ru@gmail.com',
            [sub.email],
            fail_silently=False,
        )

@app.task
def send_email_new_service(*args, **kwargs):
    for sub in Subscriber.objects.all():
        send_mail(
            'Добавлена услуга',
            f"Добавлена услуга  под названием {args}."
            f"Для просмотра перейдите на наш сайт.",
            'nopro.ru@gmail.com',
            [sub.email],
            fail_silently=False,
        )
