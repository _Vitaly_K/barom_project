from django.urls import path
from . import views


urlpatterns = [
    path('subscribe/', views.SubscribingAPIView.as_view()),
]
