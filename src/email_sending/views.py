from rest_framework.generics import CreateAPIView

from .models import Subscriber
from .serializers import SubscriberEmailSerializer


class SubscribingAPIView(CreateAPIView):
    """Добавление email для рассылки"""

    queryset = Subscriber.objects.all()
    serializer_class = SubscriberEmailSerializer
